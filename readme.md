# Plerk Test

### Pre-requisitos 📋

```
Python 3.8 +
PostgreSQL 12
```

### Instalación [Linux] 🔧

```
1 - pipenv install
```
```
2 - cp .envfile.example .envfile
```

```
3 - Configure .envfile with your environment variables
```

```
4 - python3 -m pipenv run python manage.py migrate
```

```
5 - python3 -m pipenv run python manage.py runserver
```

## Construido con 🛠️

* [Django 3.0](https://www.djangoproject.com/)
* [Rest Framework](https://www.django-rest-framework.org/)
* [PostgreSQL 12](https://www.postgresql.org/about/news/postgresql-12-released-1976/) - Manejador de dependencias

## Licencia 📄

Este proyecto está bajo la Licencia (BSD) - mira el archivo [LICENSE](LICENSE) para detalles
