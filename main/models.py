#Models
from django.db import models

class CommonInfo(models.Model):
  """ model default"""
  
  is_deleted = models.BooleanField(default=False)
  deleted_at = models.DateTimeField(null=True)
  created_at = models.DateTimeField(auto_now_add=True)
  updated_at = models.DateTimeField(auto_now=True)
  
  class Meta:
    abstract = True
