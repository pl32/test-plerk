from django.urls import path

from accounts.views import CompanyView

urlpatterns = [
    path('company/<slug:id>', CompanyView.as_view(), name='company')
]
