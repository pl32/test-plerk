
# Rest Framework
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.response import Response

# Models
from accounts.models import Company as CompanyModel
from django.db.models import Count
from django.db.models.functions import TruncDay

# Debug

class CompanyView(APIView):
    """ View company information """
    def get(self, request, *args, **kwargs):
        """ 
        Kwargs:
        
        id = id of company 
        
        """
        company_id = kwargs['id']

        company = CompanyModel.objects.filter(pk = company_id)

        if len(company) > 0:
        
            data = {
                "name": str,
                "total_transaction_charges_true": float,
                "total_transaction_charges_false": float,
                "day_max_transaction": str,
            }
            
            data["name"]= company.first().name

            data["total_transaction_charges_true"]= company.filter(transaction__final_charge = True)\
                .aggregate(total=Count("transaction__id"))["total"]

            data["total_transaction_charges_false"]= company.filter(transaction__final_charge = False)\
                .aggregate(total=Count("transaction__id"))["total"] 


            data["day_max_transaction"]=company.annotate(day=TruncDay('transaction__date'))\
                .annotate(day_max_transaction=Count('day'))\
                .order_by('-day_max_transaction')\
                .first().day.strftime("%Y/%m/%d")

            return Response({"errors": False, "code": 200, "data":data})
        else:
            return Response({"errors": True, "messages": "Company not found", "code": "404"})
