# Django
from django.db import models
from main.models import CommonInfo

class Company(CommonInfo):
  """ Company model """
  id = models.BigAutoField(primary_key=True)

  name = models.CharField(
    null=False,
    max_length=50
  )

  status = models.BooleanField(default=False, null=False)