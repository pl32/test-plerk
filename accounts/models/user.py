from django.db import models
from django.contrib.auth.models import AbstractUser
from main.models import CommonInfo

class UserProfile(AbstractUser):
  """User profile model"""
  email = models.EmailField(blank=False, max_length=254,unique=True, verbose_name="email address")

  is_deleted = models.BooleanField(default=False)
  deleted_at = models.DateTimeField(null=True)
  updated_at = models.DateTimeField(auto_now=True)
  
  USERNAME_FIELD = "email"
  REQUIRED_FIELDS = ["name"]

  class Meta:
    ordering = ("email",)
