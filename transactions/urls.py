from django.urls import path, include

from transactions.views import TransactionView

urlpatterns = [
    path('transactions', TransactionView.as_view(), name='transactions')
]
