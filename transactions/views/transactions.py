# Rest Framework
from rest_framework.views import APIView
from rest_framework.response import Response


# Models
from accounts.models import Company as CompanyModel
from transactions.models import Transaction as TransactionModel

# Utils
from django.db.models import Count, Sum

# Debug
import pdb

class TransactionView(APIView):
    """ View transaction """
    def get(self, request, *args, **kwargs):
        data = {
            "company_higgest_sales": str,
            "company_lowest_sales": str,
            "price_total_transaction_charges_true": float,
            "price_total_transaction_charges_false": float,
            "company_charges_false": str
        }

        # Get companies with status transaction closed
        companies= CompanyModel.objects.filter(transaction__status_transaction='CLOSED').annotate(sales=Count("id")).order_by('-sales')
        
        data["company_higgest_sales"]= companies.first().name
        
        data["company_lowest_sales"]= companies.last().name

        data["price_total_transaction_charges_true"]=TransactionModel.objects.filter(final_charge=True)\
            .aggregate(total = Sum('price'))['total']

        data["price_total_transaction_charges_false"]=TransactionModel.objects.filter(final_charge=False)\
            .aggregate(total = Sum('price'))['total']

        data["company_charges_false"]=CompanyModel.objects.filter(transaction__final_charge=False)\
            .annotate(rejected=Count("id")).order_by('-rejected').first().name
        
        return Response({"errors": False, "code": 200, "data":data})