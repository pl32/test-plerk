# Django
from django.db import models
from main.models import CommonInfo

from decimal import Decimal


class Transaction(CommonInfo):
  """ transaction model """
  id = models.BigAutoField(primary_key=True)

  company = models.ForeignKey('accounts.Company', related_name='transaction', on_delete=models.CASCADE, null=True)

  price = models.FloatField(null=False, default=Decimal("0.0"))

  date = models.DateTimeField(auto_now_add=True)
  
  status_transaction =  models.CharField(
    null=False,
    default='PENDING',
    max_length=14,
    choices=[
      ('CLOSED', 'closed transaction'),
      ('REVERSED', 'reversed transaction'),
      ('PENDING', 'pending transaction'),
    ]
  )
  
  status_approved = models.BooleanField(default=False, null=False)

  final_charge = models.BooleanField(default=False, null=False)
  